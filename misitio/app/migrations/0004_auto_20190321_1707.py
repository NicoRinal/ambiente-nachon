# Generated by Django 2.1.7 on 2019-03-21 17:07

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0003_auto_20190319_1657'),
    ]

    operations = [
        migrations.CreateModel(
            name='Sensor',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('Aire', models.CharField(max_length=200)),
                ('Temperatura', models.CharField(max_length=200)),
                ('Calidad', models.CharField(max_length=200)),
            ],
        ),
        migrations.DeleteModel(
            name='Pet',
        ),
        migrations.AlterField(
            model_name='classroom',
            name='sensor',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='app.Sensor'),
        ),
    ]
