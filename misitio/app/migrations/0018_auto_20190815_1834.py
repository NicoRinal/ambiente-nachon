# Generated by Django 2.1.7 on 2019-08-15 18:34

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0017_auto_20190815_1832'),
    ]

    operations = [
        migrations.RenameModel(
            old_name='Coords',
            new_name='Coord',
        ),
    ]
