# Generated by Django 2.1.7 on 2019-08-15 18:32

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0016_auto_20190815_1711'),
    ]

    operations = [
        migrations.CreateModel(
            name='Coords',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('coordX', models.CharField(max_length=200)),
                ('coordY', models.CharField(max_length=200)),
            ],
        ),
        migrations.RemoveField(
            model_name='classroom',
            name='coords',
        ),
        migrations.AddField(
            model_name='coords',
            name='aula',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='app.Classroom'),
        ),
    ]
