from django.contrib import admin
from django.contrib.admin.sites import AdminSite
from .models import Classroom, Sensor, Datos_Sensor, Coord
from django.db import models

@admin.register(Sensor)
class SensorAdmin(admin.ModelAdmin):
    list_display = ("MAC_Sensor",)
    search_fields=("MAC_Sensor",)
    group_by=("MAC_Sensor",)

@admin.register(Datos_Sensor)
class Datos_SensorAdmin(admin.ModelAdmin):
    list_display = ("Humedad", "Temperatura", "Calidad_Aire", "sensor_Mac","fechaHora",)
    search_fields = ("sensor_Mac",)

    def sensor_Mac(self, obj):
        return obj.sensor.MAC_Sensor
    def nroAula (self,obj):
        return obj.aula.number

@admin.register(Classroom)
class ClassroomAdmin(admin.ModelAdmin):
    list_display = ("number","sensor_Mac","polygonCoords")
    search_fields = ("sensor_Mac",)

    def sensor_Mac(self, obj):
        return obj.sensor.MAC_Sensor

@admin.register(Coord)
class CoordAdmin(admin.ModelAdmin):
    list_display = ("coordX","coordY","aula")
    search_fields = ("aula",)

    def aula(self,obj):
        return obj.aula.number

