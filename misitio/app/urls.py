from django.urls import path
from django.conf.urls import include, url
from . import views
from django.contrib import admin
from django.contrib.auth.views import LoginView


urlpatterns = [
    path('', views.home, name='Home'),
    path('registration/register', views.signup, name='Register'),
    path('accounts/', include('django.contrib.auth.urls')),
    path('aulas/', views.aulas, name='Aulas'),
    path('aulaX/', views.aulaX, name="Aula"),
    path('getCSV/', views.getCSV, name="getCSV"),
    path('getLastData', views.getLastData, name='getLastData'),
    path('getAulas',views.getAulas, name='getAulas')
]