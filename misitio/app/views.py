from django.shortcuts import render
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth import authenticate
from django.contrib.auth import login
import csv
from django.core.paginator import Paginator
from django.shortcuts import render
from django.utils import timezone
import datetime

from django.shortcuts import redirect
from .models import *

from django.http import HttpResponse
from .models import Sensor, Classroom, Coord

def home(request):
    if request.user.is_authenticated:
        return redirect("/aulas")

    return render(request, 'static_pages/home.html',)

def aulaX(request):

    if not request.user.is_authenticated:
        return redirect("/")

    id = request.GET.get('id')
    print(id)


    page = request.GET.get('page')

    aula = Classroom.objects.filter(number=id)[0]
    datos = Datos_Sensor.objects.filter(sensor=aula.sensor).order_by('-fechaHora')
    paginator = Paginator(datos, 25) # Show 25 contacts per page
    last = Datos_Sensor.objects.filter(sensor=aula.sensor).order_by('-id')[0]

    page = request.GET.get('page')
    manueh = paginator.get_page(page)

    return render(request, 'static_pages/aulaX.html', {'aula': aula, 'datos': datos, 'ultimoDato': last, 'manueh': manueh})


def getLastData(request):

    id = request.GET.get('id')
    print(id)

    aula = Classroom.objects.filter(number=id)[0]
    dato = Datos_Sensor.objects.filter(sensor=aula.sensor).order_by('-id')[0]

    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename="somefileLastData.csv'

    writer = csv.writer(response)
    writer.writerow(['Calidad_Aire'])

    lastData = dato.Calidad_Aire

    writer.writerow([lastData])

    return response


def aulas(request):

    if not request.user.is_authenticated:
        return redirect("/")

    listaClassroom = Classroom.objects.all()
    coordenadas = Coord.objects.all()

    stringPoligon = ""

    for item in listaClassroom:
        stringPoligon = ""
        stringPoligon += "M"
        for item2 in coordenadas:
            if item == item2.aula:
                stringPoligon += item2.coordX
                stringPoligon += " "
                stringPoligon += item2.coordY
                stringPoligon += " L"
        classroom = Classroom.objects.get(number=item.number)
        classroom.polygonCoords = stringPoligon
        print(classroom.polygonCoords)
        classroom.save()


    return render(request, 'static_pages/aulas.html', {'lista': listaClassroom})


def signup(request):
    if request.method == 'POST':
        form = UserCreationForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            raw_password = form.cleaned_data.get('password1')
            user = authenticate(username=username, password=raw_password)
            login(request, user)
            return render(request,'static_pages/aulas.html')
    else:
        form = UserCreationForm()
    return render(request, 'registration/register.html', {'form': form})

def getCSV(request):
    id = request.GET.get('id')
    print(id)
    print("cacona")

    aula = Classroom.objects.filter(number=id)[0]
    datos = Datos_Sensor.objects.filter(sensor=aula.sensor)

    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename="somefilename.csv'

    writer = csv.writer(response)
    writer.writerow(['fechaHora', 'Temperatura'])

    for item in datos:
        writer.writerow([item.fechaHora, item.Temperatura])

    return response

def getAulas(request):
    aulas = Classroom.objects.all()

    response = HttpResponse(content_type="text/csv")
    response['Content-Disposition'] = 'attachment; filename="aulas.csv'

    writer = csv.writer(response)
    writer.writerow(['nroAula','CalidadAire','polygonCoords'])

    for item in aulas:
        dato = Datos_Sensor.objects.filter(sensor=item.sensor).order_by('-id')[0]

        writer.writerow([item.number,dato.Calidad_Aire,item.polygonCoords])

    return response



