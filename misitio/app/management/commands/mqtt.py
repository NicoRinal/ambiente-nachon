from ...models import *
import paho.mqtt.client as mqtt
import json
from django.utils import timezone
import datetime
from django.core.management.base import BaseCommand, CommandError
from django.core.mail import send_mail
from django.core.mail import send_mail
from django.conf import settings

def on_connect(client, userdata, flags, rc):

    print("Connected with result code " + str(rc))
    client.subscribe("TP")


def on_message(client, userdata, msg):
    m_decode = str(msg.payload.decode("utf-8", "ignore"))
    print("data Received type", type(m_decode))
    print("data Received", m_decode)
    print("Converting from Json to Object")
    m_in = json.loads(m_decode)
    print(type(m_in))
    print(m_in["MAC"])
    sensor1 = Sensor.objects.filter(MAC_Sensor=m_in["MAC"])[0]

    dato = Datos_Sensor.objects.filter(sensor=sensor1).order_by('-id')[0]



    if m_in["sensorGas"] > (int(dato.Calidad_Aire)+10) or m_in["sensorGas"] < (int(dato.Calidad_Aire)-10) :

        datos1 = Datos_Sensor()
        datos1.Humedad = m_in["humedad"]
        datos1.Calidad_Aire = m_in["sensorGas"]
        datos1.Temperatura = m_in["temperatura"]
        datos1.sensor = sensor1
        datos1.fechaHora = timezone.now()
        datos1.save()

    if m_in["sensorGas"] >= 100:
        send_mail('POSIBLE PERDIDA DE GAS', 'Se detecto una posible fuga de gas en su ambiente, chequee todas las llaves de paso.', settings.EMAIL_HOST_USER, ['ignaciosantin59@gmail.com'], fail_silently=False)
        datos1 = Datos_Sensor()
        datos1.Humedad = m_in["humedad"]
        datos1.Calidad_Aire = m_in["sensorGas"]
        datos1.Temperatura = m_in["temperatura"]
        datos1.sensor = sensor1
        datos1.fechaHora = timezone.now()
        datos1.save()


    ultimaFecha = Datos_Sensor.objects.filter(sensor=sensor1).filter(fechaHora__gte=timezone.now()-datetime.timedelta(seconds=10))
    print(ultimaFecha)
    if len(ultimaFecha)==0:
        datos1 = Datos_Sensor()
        datos1.Humedad=m_in["humedad"]
        datos1.Calidad_Aire=m_in["sensorGas"]
        datos1.Temperatura=m_in["temperatura"]
        datos1.sensor=sensor1
        datos1.fechaHora=datetime.datetime.now()
        datos1.save()


class Command(BaseCommand):
    def handle(self, *args, **options):
        client = mqtt.Client()
        client.connect("172.16.0.135", 1883, 60)

        client.on_connect = on_connect
        client.on_message = on_message
        client.loop_forever()

