from django.db import models
from django.utils import timezone



class Sensor(models.Model):
    MAC_Sensor = models.CharField(max_length=200)
    def __str__(self):
        return self.MAC_Sensor

    def save(self):
        sasa = self.pk is None

        super(Sensor, self).save()
        if sasa:
            datos1 = Datos_Sensor()
            datos1.Humedad = 0
            datos1.Calidad_Aire = 0
            datos1.Temperatura = 0
            datos1.sensor = self
            datos1.fechaHora = timezone.now()
            datos1.save()

class Datos_Sensor(models.Model):
    Humedad = models.CharField(max_length=200)
    Temperatura = models.CharField(max_length=200)
    Calidad_Aire = models.CharField(max_length=200)
    fechaHora = models.DateTimeField()
    sensor = models.ForeignKey("Sensor",on_delete=models.CASCADE)

class Classroom(models.Model):
    number = models.CharField(max_length=200)
    polygonCoords = models.CharField(max_length=200)
    sensor = models.ForeignKey("Sensor",on_delete=models.CASCADE)

    def __str__(self):
        return "Aula " + self.number

class User(models.Model):
    name = models.CharField(max_length=200)
    password = models.CharField(max_length=200)

class Coord(models.Model):
    coordX = models.CharField(max_length=200)
    coordY = models.CharField(max_length=200)
    aula = models.ForeignKey("Classroom",on_delete=models.CASCADE)