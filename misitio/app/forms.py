from django import forms
from django.urls import reverse
from crispy_forms.bootstrap import Field
from crispy_forms.helper import FormHelper
from django.contrib.auth.models import User

from crispy_forms.layout import Submit, Layout, Div, Fieldset


class UsersForm(forms.ModelForm):
    class Meta:
        model= User
        fields = ('username', 'email', 'password')

    def __init__(self, *args, **kwargs):
        super(UsersForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_id = 'id-personal-data-form'
        self.helper.form_method = 'post'
        self.helper.form_action = reverse('submit_form')
        self.helper.add_input(Submit('submit', 'Submit', css_class='btn-success'))
        self.helper.form_class = 'form-horizontal'
        self.helper.layout = Layout(
            Fieldset('Name',Field('username', placeholder='Username',css_class="some-class"),Div('email', title="E-Mail"),),)


